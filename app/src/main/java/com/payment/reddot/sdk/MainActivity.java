package com.payment.reddot.sdk;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.payment.reddot.android_library_rdp.PaymentPageActivity;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private Button btnPay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPay = (Button) findViewById(R.id.btnPay);
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, String> parameters = new HashMap<String, String>();
                //parameters.put("order_id", "test_123");
                //parameters.put("mid", "1000089533");
                //parameters.put("api_mode", "direct_token_api");
                //parameters.put("type", "R"); // R for remove
                //parameters.put("token_id", "4730471926636132601013");
                parameters.put("merchant_reference", "merchant & reference");
                parameters.put("amount", "1.00");
                parameters.put("currency_code", "sgd");
                parameters.put("email", "herman@reddotpayment.com");
                parameters.put("first_name", "test");
                parameters.put("key", "13f0eaf0218c2886a564991dc0791563a8120757");
                parameters.put("last_name", "abc");
                parameters.put("merchant_id", "1000089147");
                parameters.put("notify_url", "http://test.reddotpayment.com/BSIMULATOR/BBLSimulator/server/notif/notif-url.php");
                parameters.put("order_number", "TEST103791");
                parameters.put("return_url", "http://test.reddotpayment.com/BSIMULATOR/BBLSimulator/server/redirect/return-url.php");
                parameters.put("transaction_type", "Sale");

                Intent i = new Intent(MainActivity.this, PaymentPageActivity.class);
                i.putExtra("parameters", parameters);
                //UUx7cOvokGVFk8bp8sVj8r4hK359A9aW96Pss9hSPHDHlY69xINjLmipYm9096nbVt7szG4E7pqXJCeFcbgj6ZGZ1HESwXQneiRmYPHe9PxM2kmGCt0PxZLP409Cfmyq
                i.putExtra("secret_key", "REDDOT");
                i.putExtra("environment", "test");
                startActivityForResult(i, 2302);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2302 && resultCode == RESULT_OK && data != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Payment Success");
            builder.setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
            Log.e("RESULT_OK", "Result Paid : " + data.getExtras().getSerializable("response"));
        } else if (requestCode == 2302 && resultCode == RESULT_CANCELED && data != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Payment Rejected");
            builder.setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
            Log.e("RESULT_CANCELLED", "Result Rejected : " + data.getExtras().getSerializable("response"));
        } else {
            //close popup will arrived here
        }
    }
}
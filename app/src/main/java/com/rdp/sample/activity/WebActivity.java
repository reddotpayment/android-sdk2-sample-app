package com.rdp.sample.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.text.TextUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.rdp.sample.BaseActivity;
import com.rdp.sample.R;
import com.rdp.sample.databinding.ActivityWebBinding;


/**
 * @author yb
 */
public class WebActivity extends BaseActivity {

    private ActivityWebBinding binding;
    private String targetUrl;

    public static void execute(Activity context, String targetUrl, String title) {
        Intent intent = new Intent(context, WebActivity.class);
        intent.putExtra(BaseActivity.Key.TARGET_URL.name(), targetUrl);
        intent.putExtra(BaseActivity.Key.TITLE.name(), title);
        context.startActivity(intent);
    }

    @Override
    protected void setRootView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_web);
    }

    @Override
    protected void initData() {
        super.initData();
        targetUrl = this.getIntent().getStringExtra(BaseActivity.Key.TARGET_URL.name());
        if (TextUtils.isEmpty(targetUrl)) {
            finish();
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void initWidget() {
        super.initWidget();

        String title = this.getIntent().getStringExtra(BaseActivity.Key.TITLE.name());
        binding.title.setText(TextUtils.isEmpty(title) ? "Title" : title);
        binding.webView.getSettings().setDomStorageEnabled(true);
        binding.webView.getSettings().setLoadWithOverviewMode(true);
        binding.webView.getSettings().setUseWideViewPort(true);
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        binding.webView.loadUrl(targetUrl);

        binding.webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {

                super.onPageFinished(view, url);
            }
        });
        binding.back.setOnClickListener(v -> back());
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    private void back() {
        if (binding.webView != null && binding.webView.canGoBack()) {
            binding.webView.goBack();
            return;
        }
        finish();
    }
}

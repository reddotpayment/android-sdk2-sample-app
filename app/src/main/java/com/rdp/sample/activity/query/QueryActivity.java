package com.rdp.sample.activity.query;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.payment.reddot.library_rdp.entity.QueryResponse;
import com.payment.reddot.library_rdp.main.GenerateRequestJson;
import com.payment.reddot.library_rdp.service.ServiceManager;
import com.payment.reddot.library_rdp.service.ServiceUtils;
import com.rdp.sample.ACacheUtil;
import com.rdp.sample.BaseActivity;
import com.rdp.sample.CommonMessageDialog;
import com.rdp.sample.R;
import com.rdp.sample.databinding.ActivityQueryBinding;

import rx.Observable;


/**
 * @author yb
 */
public class QueryActivity extends BaseActivity {
    private ActivityQueryBinding binding;
    public static final String SECRET_KEY = "RedDot";
    private Type type;

    public enum Type {
        REDIRECTION, PAYMENT_REDIRECTION,
    }

    public static void execute(Activity context, Type type) {
        Intent intent = new Intent(context, QueryActivity.class);
        intent.putExtra(Key.TYPE.name(), type);
        context.startActivity(intent);
    }

    @Override
    protected void setRootView() {
        super.setRootView();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_query);
    }

    @Override
    protected void initData() {
        super.initData();
        type = (Type) getIntent().getSerializableExtra(Key.TYPE.name());
    }

    @Override
    protected void initWidget() {
        super.initWidget();
        binding.back.setOnClickListener(v -> finish());
        String asString = ACacheUtil.getInstance().getaCache().getAsString(Key.TRANSACTION_ID.name());
        binding.transactionId.setText(asString);
        binding.secretKey.setText(SECRET_KEY);
        binding.confirm.setOnClickListener(v -> {
            showProgressDialog("Loading");
            String transactionId = binding.transactionId.getText().toString();
            String requestMid = binding.requestMid.getText().toString();
            String queryPaymentResult =  GenerateRequestJson.getInstance().getQueryPaymentResult(transactionId, requestMid,binding.secretKey.getText().toString());

            Observable<String> observable;
            if (type == Type.PAYMENT_REDIRECTION) {
                observable = ServiceManager.getInstance().getService().queryRedirection(queryPaymentResult);
            } else {
                observable = ServiceManager.getInstance().getService().query_token_redirection(queryPaymentResult);
            }
            ServiceUtils.subscribe(observable, data -> {
                hideProgressDialog();
                Gson gson = new Gson();
                QueryResponse response = gson.fromJson(data, QueryResponse.class);
                String showStr;
                if (response != null && !TextUtils.isEmpty(response.getResponse_code())) {
                    showStr = response.getResponse_code() + "\n" + data;
                } else {
                    if (response != null) {
                        showStr = response.getResponse_msg();
                    } else {
                        showStr = "error";
                    }
                }
                new CommonMessageDialog(this, showStr, "Message").show();
            }, throwable -> {
                hideProgressDialog();
                new CommonMessageDialog(this, throwable.getMessage(), "Message").show();

                Log.e("data", "====");
            });


        });
    }
}

package com.rdp.sample.activity.token;

import android.databinding.DataBindingUtil;
import android.view.View;

import com.rdp.sample.BaseActivity;
import com.rdp.sample.R;
import com.rdp.sample.activity.query.QueryActivity;
import com.rdp.sample.databinding.ActivityTokenBinding;

import static com.rdp.sample.activity.query.QueryActivity.Type.REDIRECTION;

/**
 * @author yb
 */
public class TokenActivity extends BaseActivity {

    private ActivityTokenBinding binding;

    @Override
    protected void setRootView() {
        super.setRootView();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_token);
    }

    @Override
    protected void initWidget() {
        super.initWidget();
        binding.back.setOnClickListener(v -> finish());
        binding.directCreate.setOnClickListener(v -> DirectAndHostActivity.execute(this, DirectAndHostActivity.Type.DIRECT_CREATE));
        binding.directModify.setOnClickListener(v -> DirectAndHostActivity.execute(this, DirectAndHostActivity.Type.DIRECT_MODIFY));
        binding.directRemove.setOnClickListener(v -> DirectAndHostActivity.execute(this, DirectAndHostActivity.Type.DIRECT_REMOVE));
        binding.hostCreate.setOnClickListener(v -> DirectAndHostActivity.execute(this, DirectAndHostActivity.Type.HOST_CREATE));
        binding.hostModify.setOnClickListener(v -> DirectAndHostActivity.execute(this, DirectAndHostActivity.Type.HOST_MODIFY));
        binding.rdpQueryApi.setOnClickListener(v -> QueryActivity.execute(this,REDIRECTION));
    }
}

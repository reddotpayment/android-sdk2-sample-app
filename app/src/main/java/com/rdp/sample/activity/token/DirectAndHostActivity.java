package com.rdp.sample.activity.token;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.payment.reddot.library_rdp.main.GenerateRequestJson;
import com.payment.reddot.library_rdp.service.ServiceManager;
import com.payment.reddot.library_rdp.service.ServiceUtils;
import com.payment.reddot.library_rdp.entity.TokenizationRequest;
import com.rdp.sample.ACacheUtil;
import com.rdp.sample.BaseActivity;
import com.rdp.sample.CommonMessageDialog;
import com.rdp.sample.R;
import com.rdp.sample.activity.WebActivity;
import com.rdp.sample.databinding.ActivityDirectCreatBinding;
import com.rdp.sample.vo.DirectTokenResponse;
import com.rdp.sample.vo.HostTokenResponse;

import java.io.IOException;

/**
 * @author yb
 */
public class DirectAndHostActivity extends BaseActivity {
    private ActivityDirectCreatBinding binding;
    private Type type;
    public static final String HOSTED_TOKEN_API = "hosted_token_api";
    public static final String DIRECT_TOKEN_API = "direct_token_api";

    public enum Type {
        DIRECT_CREATE, DIRECT_MODIFY, DIRECT_REMOVE, HOST_CREATE, HOST_MODIFY
    }

    public static void execute(Activity context, Type type) {
        Intent intent = new Intent(context, DirectAndHostActivity.class);
        intent.putExtra(BaseActivity.Key.TYPE.name(), type);
        context.startActivity(intent);
    }


    @Override
    protected void setRootView() {
        super.setRootView();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_direct_creat);
    }

    @Override
    protected void initData() {
        super.initData();
        type = (Type) getIntent().getSerializableExtra(Key.TYPE.name());
    }

    @Override
    protected void initWidget() {
        super.initWidget();

        initTitle();

        initUI();

        initPayerId();

        initTransactionTypeType();

        doConfirm();

        binding.orderId.setText(String.valueOf(System.currentTimeMillis()));

    }

    private void initTitle() {
        if (type == Type.HOST_CREATE) {
            binding.title.setText("Hosted Token API Create");
        } else if (type == Type.HOST_MODIFY) {
            binding.title.setText("Hosted Token API Modify");
        } else if (type == Type.DIRECT_CREATE) {
            binding.title.setText("Direct Token API Create");
        } else if (type == Type.DIRECT_MODIFY) {
            binding.title.setText("Direct Token API Modify");
        } else {
            binding.title.setText("Direct Token API Remove");
        }
        if (type == Type.HOST_MODIFY || type == Type.HOST_CREATE) {
            binding.cardNo.setVisibility(View.GONE);
            binding.expDate.setVisibility(View.GONE);
            binding.cvv2.setVisibility(View.GONE);
            binding.walletId.setVisibility(View.GONE);
        } else {
            binding.cardNo.setVisibility(View.VISIBLE);
            binding.expDate.setVisibility(View.VISIBLE);
            binding.cvv2.setVisibility(View.VISIBLE);
            binding.walletId.setVisibility(View.VISIBLE);
        }
    }

    private void initUI() {
        binding.redirectUrlContainer.setVisibility(type == Type.HOST_MODIFY || type == Type.HOST_CREATE ? View.VISIBLE : View.GONE);
        binding.backUrlContainer.setVisibility(type == Type.HOST_MODIFY || type == Type.HOST_CREATE ? View.VISIBLE : View.GONE);
        if (type == Type.HOST_CREATE || type == Type.HOST_MODIFY) {
            binding.apiMode.setText(HOSTED_TOKEN_API);
            binding.backUrl.setText(" https://dev.emuzi-tech.com/HippoStart_V3/callback/RDBCancel");
            binding.redirectUrl.setText("https://dev.emuzi-tech.com/HippoStart_V3/callback/RDPAttachSuccess");
        } else {
            binding.backUrl.setText("");
            binding.redirectUrl.setText("");
            binding.apiMode.setText(DIRECT_TOKEN_API);
        }
        binding.back.setOnClickListener(v -> finish());
    }

    private void doConfirm() {
        binding.confirm.setOnClickListener((View v) -> {
            showProgressDialog("Loading");
            binding.progressBar.setVisibility(View.VISIBLE);
            String secretKey;
            if (!TextUtils.isEmpty(binding.secretKey.getText().toString())) {
                secretKey = binding.secretKey.getText().toString();
            } else {
                secretKey = "RedDot";
            }
            TokenizationRequest request = new TokenizationRequest();
            if (!TextUtils.isEmpty(binding.mid.getText().toString())) {
                request.setMid(binding.mid.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.orderId.getText().toString())) {
                request.setOrder_id(binding.orderId.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.apiMode.getText().toString())) {
                request.setApi_mode(binding.apiMode.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.notifyUrl.getText().toString())) {
                request.setNotify_url(binding.notifyUrl.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.merchantReference.getText().toString())) {
                request.setMerchant_reference(binding.merchantReference.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.walletId.getText().toString())) {
                request.setWallet_id(binding.walletId.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToPhone.getText().toString())) {
                request.setBill_to_phone(binding.billToPhone.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToAddressPostalCode.getText().toString())) {
                request.setBill_to_address_postal_code(binding.billToAddressPostalCode.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToAddressState.getText().toString())) {
                request.setBill_to_address_state(binding.billToAddressState.getText().toString());
            }

            if (!TextUtils.isEmpty(binding.billToAddressCountry.getText().toString())) {
                request.setBill_to_address_country(binding.billToAddressCountry.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToAddressLine2.getText().toString())) {
                request.setBill_to_address_line2(binding.billToAddressLine2.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToAddressLine1.getText().toString())) {
                request.setBill_to_address_line1(binding.billToAddressLine1.getText().toString());
            }

            if (!TextUtils.isEmpty(binding.billToAddressCity.getText().toString())) {
                request.setBill_to_address_city(binding.billToAddressCity.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToSurname.getText().toString())) {
                request.setBill_to_surname(binding.billToSurname.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToForename.getText().toString())) {
                request.setBill_to_forename(binding.billToForename.getText().toString());
            }

            if (!TextUtils.isEmpty(binding.cvv2.getText().toString())) {
                request.setCvv2(binding.cvv2.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.ccy.getText().toString())) {
                request.setCcy(binding.ccy.getText().toString());
            }

            if (!TextUtils.isEmpty(binding.expDate.getText().toString())) {
                request.setExp_date(binding.expDate.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.cardNo.getText().toString())) {
                request.setCard_no(binding.cardNo.getText().toString());
            }

            if (!TextUtils.isEmpty(binding.cardNo.getText().toString())) {
                request.setCard_no(binding.cardNo.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.payerId.getText().toString())) {
                request.setPayer_id(binding.payerId.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.payerName.getText().toString())) {
                request.setPayer_name(binding.payerName.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.payerEmail.getText().toString())) {
                request.setPayer_email(binding.payerEmail.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.transactionType.getText().toString())) {
                request.setTransaction_type(binding.transactionType.getText().toString());
            } else {
                request.setType(binding.type.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.redirectUrl.getText().toString())) {
                request.setRedirect_url(binding.redirectUrl.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.backUrl.getText().toString())) {
                request.setBack_url(binding.backUrl.getText().toString());
            }

            try {
                if (type == Type.HOST_MODIFY || type == Type.HOST_CREATE) {
                    String data = GenerateRequestJson.getInstance().hostedTokenApi(request, secretKey);
                    ServiceUtils.subscribe(ServiceManager.getInstance().getService().hostTokenApi(data), str -> {
                        hideProgressDialog();
                        Gson gson = new Gson();
                        HostTokenResponse hostTokenResponse = gson.fromJson(str, HostTokenResponse.class);
                        if (hostTokenResponse != null && hostTokenResponse.getResponse_code() == 0) {
                            if (type == Type.HOST_CREATE) {
                                showCenterToast("HOST CREATE SUCCESS");
                                Log.e("data", "HOST CREATE SUCCESS");
                            } else {
                                showCenterToast("HOST MODIFY SUCCESS");
                                Log.e("data", "HOST MODIFY SUCCESS");
                            }
                            if (!TextUtils.isEmpty(hostTokenResponse.getTransaction_id())) {
                                ACacheUtil.getInstance().getaCache().put(Key.TRANSACTION_ID.name(), hostTokenResponse.getTransaction_id());
                            }
                            WebActivity.execute(this, hostTokenResponse.getPayment_url(), "");

                        } else {
                            if (hostTokenResponse != null && !TextUtils.isEmpty(hostTokenResponse.getResponse_msg())) {
                                new CommonMessageDialog(this, hostTokenResponse.getResponse_msg(), "Message").show();
                            }
                        }
                    }, throwable -> {
                        hideProgressDialog();
                        new CommonMessageDialog(this, throwable.getMessage(), "Message").show();
                        Log.e("data", "throwable");
                    });
                } else {
                    String requestData = GenerateRequestJson.getInstance().directTokenizationApi(request, secretKey);
                    ServiceUtils.subscribe(ServiceManager.getInstance().getService().directTokenApi(requestData), data -> {
                        hideProgressDialog();
                        Gson gson = new Gson();
                        DirectTokenResponse directResponse = gson.fromJson(data, DirectTokenResponse.class);
                        if (directResponse != null && directResponse.getResponse_code() == 0) {
                            String str;
                            if (type == Type.DIRECT_CREATE) {
                                str = "DIRECT CREATE SUCCESS";
                                Log.e("data", "DIRECT CREATE SUCCESS");
                                if (!TextUtils.isEmpty(directResponse.getPayer_id())) {
                                    ACacheUtil.getInstance().getaCache().put(Key.DIRECT_PAYER_ID.name(), directResponse.getPayer_id());
                                }
                            } else if (type == Type.DIRECT_MODIFY) {
                                str = "DIRECT MODIFY SUCCESS";
                                Log.e("data", "DIRECT MODIFY SUCCESS");
                            } else {
                                str = "DIRECT REMOVE SUCCESS";
                                Log.e("data", "DIRECT REMOVE SUCCESS");
                                ACacheUtil.getInstance().getaCache().remove(Key.DIRECT_PAYER_ID.name());
                            }
                            new CommonMessageDialog(this, str + "\n" + data, "Message").show();
                        } else {
                            if (directResponse != null && !TextUtils.isEmpty(directResponse.getResponse_msg())) {
                                new CommonMessageDialog(this, directResponse.getResponse_msg(), "Message").show();
                            }
                        }
                    }, throwable -> {
                        hideProgressDialog();
                        new CommonMessageDialog(this, throwable.getMessage(), "Message").show();
                        Log.e("data", "====");
                    });
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                binding.progressBar.setVisibility(View.GONE);
            }
        });

    }

    private void initTransactionTypeType() {
        if (type == Type.DIRECT_CREATE || type == Type.HOST_CREATE) {
            binding.transactionType.setText("C");
            binding.type.setText("C");
        } else if (type == Type.DIRECT_REMOVE) {
            binding.transactionType.setText("R");
            binding.type.setText("R");
        } else if (type == Type.DIRECT_MODIFY || type == Type.HOST_MODIFY) {
            binding.transactionType.setText("M");
            binding.type.setText("M");
        }
    }

    private void initPayerId() {
//        if (type == Type.DIRECT_REMOVE || type == Type.DIRECT_MODIFY || type == Type.HOST_MODIFY) {
//
//            binding.payerIdContainer.setVisibility(View.VISIBLE);
//        } else {
//            binding.payerId.setText("");
//            binding.payerIdContainer.setVisibility(View.GONE);
//        }
        binding.payerId.setText(ACacheUtil.getInstance().getaCache().getAsString(Key.DIRECT_PAYER_ID.name()));
    }
}

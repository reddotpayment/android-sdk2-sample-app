package com.rdp.sample.activity.payment;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.payment.reddot.library_rdp.main.GenerateRequestJson;
import com.payment.reddot.library_rdp.service.ServiceManager;
import com.payment.reddot.library_rdp.service.ServiceUtils;
import com.payment.reddot.library_rdp.entity.RedirectApiRequest;
import com.rdp.sample.ACacheUtil;
import com.rdp.sample.BaseActivity;
import com.rdp.sample.CommonMessageDialog;
import com.rdp.sample.R;
import com.rdp.sample.StringUtils;
import com.rdp.sample.activity.WebActivity;
import com.rdp.sample.databinding.ActivityRedirectBinding;
import com.rdp.sample.vo.RedirectPaymentResponse;

/**
 * @author yb
 */
public class RedirectPayActivity extends BaseActivity {
    public static final String DIRECT_PAY_API = "redirection_hosted";
    private ActivityRedirectBinding binding;
    private Type type;

    public enum Type {
        PAYMENT_ONLY, WITH_TOKEN,
    }

    public static void execute(Activity context, Type type) {
        Intent intent = new Intent(context, RedirectPayActivity.class);
        intent.putExtra(Key.TYPE.name(), type);
        context.startActivity(intent);
    }

    @Override
    protected void setRootView() {
        super.setRootView();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_redirect);
    }

    @Override
    protected void initData() {
        super.initData();
        type = (Type) getIntent().getSerializableExtra(Key.TYPE.name());
    }

    @Override
    protected void initWidget() {
        super.initWidget();
        binding.back.setOnClickListener(v -> finish());
        binding.apiMode.setText(DIRECT_PAY_API);
        binding.tokenMod.setText(type == Type.PAYMENT_ONLY ? "0" : "1");
        binding.orderId.setText(String.valueOf(System.currentTimeMillis()));
        String payerId = ACacheUtil.getInstance().getaCache().getAsString(Key.DIRECT_PAYER_ID.name());
        if (!TextUtils.isEmpty(payerId)) {
            binding.payerId.setText(payerId);
        }
        binding.confirm.setOnClickListener(v -> {
            showProgressDialog("Loading");
            String secretKey;
            if (!TextUtils.isEmpty(binding.secretKey.getText().toString())) {
                secretKey = binding.secretKey.getText().toString();
            } else {
                secretKey = "RedDot";
            }
            if (DIRECT_PAY_API.equals(binding.apiMode.getText().toString())) {
                binding.cardNo.setText("");
                binding.cvv2.setText("");
                binding.expDate.setText("");
            }
            RedirectApiRequest request = new RedirectApiRequest();
            if (!TextUtils.isEmpty(binding.mid.getText().toString())) {
                request.setMid(binding.mid.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.backUrl.getText().toString())) {
                request.setBack_url(binding.backUrl.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.redirectUrl.getText().toString())) {
                request.setRedirect_url(binding.redirectUrl.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.notifyUrl.getText().toString())) {
                request.setNotify_url(binding.notifyUrl.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.orderId.getText().toString())) {
                request.setOrder_id(binding.orderId.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.apiMode.getText().toString())) {
                request.setApi_mode(binding.apiMode.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.ccy.getText().toString())) {
                request.setCcy(binding.ccy.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.amount.getText().toString())) {
                request.setAmount(binding.amount.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.paymentType.getText().toString())) {
                request.setPayment_type(binding.paymentType.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.payerEmail.getText().toString())) {
                request.setPayer_email(binding.payerEmail.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.merchantReference.getText().toString())) {
                request.setMerchant_reference(binding.merchantReference.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.locale.getText().toString())) {
                request.setLocale(binding.locale.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.multipleMethodPage.getText().toString())) {
                request.setMultiple_method_page(binding.multipleMethodPage.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.payerName.getText().toString())) {
                request.setPayer_name(binding.payerName.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.payerId.getText().toString())) {
                request.setPayer_id(binding.payerId.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.cardNo.getText().toString())) {
                request.setCard_no(binding.cardNo.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.expDate.getText().toString())) {
                request.setExp_date(binding.expDate.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.cvv2.getText().toString())) {
                request.setCvv2(binding.cvv2.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToSurname.getText().toString())) {
                request.setBill_to_surname(binding.billToSurname.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToForename.getText().toString())) {
                request.setBill_to_forename(binding.billToForename.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToAddressCity.getText().toString())) {
                request.setBill_to_address_city(binding.billToAddressCity.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToAddressLine2.getText().toString())) {
                request.setBill_to_address_line2(binding.billToAddressLine2.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToAddressLine1.getText().toString())) {
                request.setBill_to_address_line1(binding.billToAddressLine1.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToAddressCountry.getText().toString())) {
                request.setBill_to_address_country(binding.billToAddressCountry.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToAddressState.getText().toString())) {
                request.setBill_to_address_state(binding.billToAddressState.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToAddressPostalCode.getText().toString())) {
                request.setBill_to_address_postal_code(binding.billToAddressPostalCode.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToPhone.getText().toString())) {
                request.setBill_to_phone(binding.billToPhone.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.shipToSurname.getText().toString())) {
                request.setShip_to_surname(binding.shipToSurname.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.shipToForename.getText().toString())) {
                request.setShip_to_forename(binding.shipToForename.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.shipToAddressCity.getText().toString())) {
                request.setShip_to_address_city(binding.shipToAddressCity.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.shipToAddressLine2.getText().toString())) {
                request.setShip_to_address_line2(binding.shipToAddressLine2.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.shipToAddressLine1.getText().toString())) {
                request.setShip_to_address_line1(binding.shipToAddressLine1.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.shipToAddressCountry.getText().toString())) {
                request.setShip_to_address_country(binding.shipToAddressCountry.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.shipToAddressState.getText().toString())) {
                request.setShip_to_address_state(binding.shipToAddressState.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.shipToAddressPostalCode.getText().toString())) {
                request.setShip_to_address_postal_code(binding.shipToAddressPostalCode.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.shipToPhone.getText().toString())) {
                request.setShip_to_phone(binding.shipToPhone.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.tenorMonth.getText().toString())) {
                request.setTenor_month(binding.tenorMonth.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.tokenMod.getText().toString())) {
                request.setToken_mod(binding.tokenMod.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.tokenModId.getText().toString())) {
                request.setToken_mod_id(binding.tokenModId.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.paymentChannel.getText().toString())) {
                request.setPayment_channel(binding.paymentChannel.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.storeCode.getText().toString())) {
                request.setStore_code(binding.storeCode.getText().toString());
            }

            String jsonData = GenerateRequestJson.getInstance().redirectPayment(request, secretKey);
            ServiceUtils.subscribe(ServiceManager.getInstance().getService().paymentApi(jsonData), data -> {
                hideProgressDialog();
                Gson gson = new Gson();
                RedirectPaymentResponse response = gson.fromJson(data, RedirectPaymentResponse.class);
                if (response != null && response.getResponse_code() == 0) {
                    if (!TextUtils.isEmpty(response.getPayment_url())) {
                        WebActivity.execute(this, response.getPayment_url(), "");
                    }
                    if (!TextUtils.isEmpty(response.getTransaction_id())) {
                        ACacheUtil.getInstance().getaCache().put(Key.TRANSACTION_ID.name(), response.getTransaction_id());
                    }
                } else {
                    if (response != null && !TextUtils.isEmpty(response.getResponse_msg())) {
                        new CommonMessageDialog(this, response.getResponse_msg(), "Message").show();
                    }
                }
            }, throwable -> {
                hideProgressDialog();
                new CommonMessageDialog(this, throwable.getMessage(), "Message").show();
                Log.e("data", "====");
            });
        });
    }
}

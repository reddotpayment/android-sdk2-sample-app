package com.rdp.sample.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.rdp.sample.R;
import com.rdp.sample.activity.payment.PaymentActivity;
import com.rdp.sample.activity.token.TokenActivity;

/**
 * @author yb
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button tokenButton = findViewById(R.id.token);
        Button payment = findViewById(R.id.payment);
        tokenButton.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, TokenActivity.class)));
        payment.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, PaymentActivity.class)));
    }
}
package com.rdp.sample.activity.payment;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.google.gson.*;
import com.payment.reddot.library_rdp.main.GenerateRequestJson;
import com.payment.reddot.library_rdp.service.ServiceManager;
import com.payment.reddot.library_rdp.service.ServiceUtils;
import com.payment.reddot.library_rdp.entity.DirectApiRequest;
import com.rdp.sample.ACacheUtil;
import com.rdp.sample.BaseActivity;
import com.rdp.sample.CommonMessageDialog;
import com.rdp.sample.R;
import com.rdp.sample.StringUtils;
import com.rdp.sample.databinding.ActivityDirectPayBinding;
import com.rdp.sample.vo.DirectPaymentResponse;

import java.io.IOException;

/**
 * @author yb
 */
public class DirectPayActivity extends BaseActivity {

    private Type type;

    public enum Type {
        PAYMENT_ONLY, WITH_TOKEN,
    }

    public static final String DIRECT_PAY_API = "direct_n3d";
    private ActivityDirectPayBinding binding;

    public static void execute(Activity context, Type type) {
        Intent intent = new Intent(context, DirectPayActivity.class);
        intent.putExtra(Key.TYPE.name(), type);
        context.startActivity(intent);
    }

    @Override
    protected void setRootView() {
        super.setRootView();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_direct_pay);
    }

    @Override
    protected void initData() {
        super.initData();
        type = (Type) getIntent().getSerializableExtra(Key.TYPE.name());

    }

    @Override
    protected void initWidget() {
        super.initWidget();
        binding.orderId.setText(String.valueOf(System.currentTimeMillis()));
        String payerId = ACacheUtil.getInstance().getaCache().getAsString(Key.DIRECT_PAYER_ID.name());
        if (!TextUtils.isEmpty(payerId)) {
            binding.payerId.setText(payerId);
//            binding.tokenId.setText(payerId);
        }
        binding.tokenMode.setText(type == Type.PAYMENT_ONLY ? "0" : "1");

        binding.back.setOnClickListener(v -> finish());

        binding.confirm.setOnClickListener((View v) -> {
            showProgressDialog("Loading");
            DirectApiRequest request = new DirectApiRequest();
            if (!TextUtils.isEmpty(binding.mid.getText().toString())) {
                request.setMid(binding.mid.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.orderId.getText().toString())) {
                request.setOrder_id(binding.orderId.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.amount.getText().toString())) {
                request.setAmount(binding.amount.getText().toString());
            }
            request.setApi_mode(DIRECT_PAY_API);
            if (!TextUtils.isEmpty(binding.cardNo.getText().toString())) {
                request.setCard_no(binding.cardNo.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.ccy.getText().toString())) {
                request.setCcy(binding.ccy.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.paymentType.getText().toString())) {
                // S I A
                request.setPayment_type(binding.paymentType.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.merchantReference.getText().toString())) {
                request.setMerchant_reference(binding.merchantReference.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.cvv2.getText().toString())) {
                request.setCvv2(binding.cvv2.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.expDate.getText().toString())) {
                request.setExp_date(binding.expDate.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.cardNo.getText().toString())) {
                request.setCard_no(binding.cardNo.getText().toString());
            }

            if (!TextUtils.isEmpty(binding.payerId.getText().toString()) && TextUtils.isEmpty(binding.cardNo.getText())) {
                request.setPayer_id(binding.payerId.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.payerName.getText().toString())) {
                request.setPayer_name(binding.payerName.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.payerEmail.getText().toString())) {
                request.setPayer_email(binding.payerEmail.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.tenorMonth.getText().toString())) {
                request.setTenor_month(binding.tenorMonth.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.clientIpAddress.getText().toString())) {
                request.setClient_ip_address(binding.clientIpAddress.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.clientUserAgent.getText().toString())) {
                request.setClient_ip_address(binding.clientIpAddress.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.tokenMode.getText().toString())) {
                request.setToken_mod(binding.tokenMode.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.tokenId.getText().toString())) {
                request.setToken_id(binding.tokenId.getText().toString());
            }

            if (!TextUtils.isEmpty(binding.billToPhone.getText().toString())) {
                request.setBill_to_phone(binding.billToPhone.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToAddressPostalCode.getText().toString())) {
                request.setBill_to_address_postal_code(binding.billToAddressPostalCode.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToAddressState.getText().toString())) {
                request.setBill_to_address_state(binding.billToAddressState.getText().toString());
            }

            if (!TextUtils.isEmpty(binding.billToAddressCountry.getText().toString())) {
                request.setBill_to_address_country(binding.billToAddressCountry.getText().toString());
            }

            if (!TextUtils.isEmpty(binding.billToAddressLine1.getText().toString())) {
                request.setBill_to_address_line1(binding.billToAddressLine1.getText().toString());
            }

            if (!TextUtils.isEmpty(binding.billToAddressCity.getText().toString())) {
                request.setBill_to_address_city(binding.billToAddressCity.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToSurname.getText().toString())) {
                request.setBill_to_surname(binding.billToSurname.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToForename.getText().toString())) {
                request.setBill_to_forename(binding.billToForename.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.tokenModId.getText().toString())) {
                request.setToken_mod_id(binding.tokenModId.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.notifyUrl.getText().toString())) {
                request.setNotify_url(binding.notifyUrl.getText().toString());
            }
            if (!TextUtils.isEmpty(binding.billToAddressLine2.getText().toString())) {
                request.setBill_to_address_line2(binding.billToAddressLine2.getText().toString());
            }
            String secretKey;
            if (!TextUtils.isEmpty(binding.secretKey.getText().toString())) {
                secretKey = binding.secretKey.getText().toString();
            } else {
                secretKey = "RedDot";
            }
            try {
                String jsonData = GenerateRequestJson.getInstance().directPayment(request, secretKey);
                ServiceUtils.subscribe(ServiceManager.getInstance().getService().paymentApi(jsonData), data -> {
                    hideProgressDialog();
                    Gson gson = new Gson();
                    DirectPaymentResponse response = gson.fromJson(data, DirectPaymentResponse.class);
                    if (response != null && response.getResponse_code() == 0) {
                        new CommonMessageDialog(this, response.getResponse_msg() + "\n" + data, "Message").show();
                        if (!TextUtils.isEmpty(response.getTransaction_id())) {
                            ACacheUtil.getInstance().getaCache().put(Key.TRANSACTION_ID.name(), response.getTransaction_id());
                        }
                        if (!TextUtils.isEmpty(response.getPayer_id())) {
                            ACacheUtil.getInstance().getaCache().put(Key.DIRECT_PAYER_ID.name(), response.getPayer_id());
                        }
                    } else {
                        if (response != null && !TextUtils.isEmpty(response.getResponse_msg())) {
                            new CommonMessageDialog(this, response.getResponse_msg(), "Message").show();
                        }
                    }

                }, throwable -> {
                    hideProgressDialog();
                    new CommonMessageDialog(this, throwable.getMessage(), "Message").show();

                    Log.e("data", "====");
                });

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}

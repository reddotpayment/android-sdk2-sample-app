package com.rdp.sample.activity.payment;

import android.databinding.DataBindingUtil;

import com.rdp.sample.BaseActivity;
import com.rdp.sample.R;
import com.rdp.sample.activity.query.QueryActivity;
import com.rdp.sample.databinding.ActivityPaymentBinding;

/**
 * @author yb
 */
public class PaymentActivity extends BaseActivity {
    private ActivityPaymentBinding binding;

    @Override
    protected void setRootView() {
        super.setRootView();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_payment);
    }

    @Override
    protected void initWidget() {
        super.initWidget();
        binding.back.setOnClickListener(v -> finish());
        binding.directPaymentWithTokenizationTogether.setOnClickListener(v -> DirectPayActivity.execute(this, DirectPayActivity.Type.WITH_TOKEN));
        binding.redirectPaymentWithTokenizationTogether.setOnClickListener(v -> RedirectPayActivity.execute(this,RedirectPayActivity.Type.WITH_TOKEN));
        binding.directPaymentOnly.setOnClickListener(v -> DirectPayActivity.execute(this, DirectPayActivity.Type.PAYMENT_ONLY));
        binding.redirectPaymentOnly.setOnClickListener(v -> RedirectPayActivity.execute(this,RedirectPayActivity.Type.PAYMENT_ONLY));
        binding.rdpQueryApi.setOnClickListener(v -> QueryActivity.execute(this, QueryActivity.Type.PAYMENT_REDIRECTION));
    }
}

package com.rdp.sample;


import android.content.Context;

/**
 * @author yb
 * @date 2018/3/6
 */
public class ACacheUtil {
    private static volatile ACacheUtil ourInstance = null;

    public static ACacheUtil getInstance() {
        if (ourInstance == null) {
            synchronized (ACacheUtil.class) {
                if (ourInstance == null) {
                    ourInstance = new ACacheUtil();
                }
            }
        }
        return ourInstance;
    }

    private ACache aCache;

    public void init(Context context) {
        aCache = buildService(context);
    }

    public ACache getaCache() {
        return aCache;
    }

    private ACache buildService(Context context) {
        return ACache.get(context);
    }

}

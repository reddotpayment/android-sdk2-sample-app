package com.rdp.sample;

import android.app.Application;

import com.payment.reddot.library_rdp.main.Constant;
import com.payment.reddot.library_rdp.service.ServiceManager;

import java.util.Locale;


/**
 * @author yb
 * @date 2016/5/7
 */
public class MainApplication extends Application {

    private static MainApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        ACacheUtil.getInstance().init(getApplicationContext());
//        Constant.ENVIRONMENT_SANDBOX
//        Constant.ENVIRONMENT_LIVE;
        ServiceManager.getInstance().init(Constant.ENVIRONMENT_SANDBOX);
    }

    public static MainApplication getInstance() {
        return instance;
    }

    public String getLanguage() {
        String language = Locale.getDefault().getLanguage();
        if (!"zh".equals(language)) {
            language = "en";
        }
        return language;
    }
}

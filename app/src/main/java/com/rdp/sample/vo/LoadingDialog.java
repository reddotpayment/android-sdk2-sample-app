package com.rdp.sample.vo;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;

import com.rdp.sample.Helper;
import com.rdp.sample.R;
import com.rdp.sample.databinding.LayoutCommonMessageDialogBinding;

/**
 * @author 40722
 * @date 2016/7/20
 */
public class LoadingDialog {
    private Context context;
    private String message;
    private String title;
    private Dialog dialog;

    public Dialog getDialog() {
        return dialog;
    }

    public LoadingDialog(Context context, String message, String title) {
        this.context = context;
        this.message = message;
        this.title = title;

    }

    public LoadingDialog show() {
        LayoutCommonMessageDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_common_message_dialog, null, false);
        dialog = Helper.showCustomDialog(context, binding.getRoot(), 0, Gravity.CENTER);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        binding.message.setText(message);

        if (!TextUtils.isEmpty(title)) {
            binding.title.setText(title);
        }

        binding.rightClick.setOnClickListener(view -> {

            dialog.dismiss();
        });
        return null;
    }
}

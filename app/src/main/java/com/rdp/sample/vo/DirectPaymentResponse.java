package com.rdp.sample.vo;

/**
 * @author yb
 * @date 2018/3/6
 */
public class DirectPaymentResponse {

    /**
     * mid : 0000021009
     * transaction_id : 1520334145115_871207071160753051
     * order_id : 1520334145115
     * acquirer_transaction_id : 453645
     * request_amount : 0.01
     * request_ccy : SGD
     * authorized_amount : 0.01
     * authorized_ccy : SGD
     * acquirer_authorized_amount : 0.01
     * acquirer_authorized_ccy : SGD
     * response_code : 0
     * response_msg : successful
     * acquirer_response_code : 0
     * acquirer_response_msg : APPROVED OR COMPLETED
     * acquirer_authorization_code : 657300
     * created_timestamp : 2018-03-06 19:02:30
     * acquirer_created_timestamp : 2018-03-06 19:01:53
     * first_6 : 411111
     * last_4 : 1111
     * payer_name : TPayer
     * exp_date : 122022
     * request_timestamp : 2018-03-06 19:02:27
     * request_mid : 0000021009
     * transaction_type : S
     * signature : 1d10b4940ad76123a77ad1edd5539d2d9891bbf8dc9fe2d4847e3dcc6284a89303ca1866964b10e3c3f60efb740103b30a14017f9ac8cf0b836720b8b4704727
     */

    private String mid;
    private String payer_id;
    private String transaction_id;
    private String order_id;
    private String acquirer_transaction_id;
    private String request_amount;
    private String request_ccy;
    private String authorized_amount;
    private String authorized_ccy;
    private String acquirer_authorized_amount;
    private String acquirer_authorized_ccy;
    private int response_code;
    private String response_msg;
    private String acquirer_response_code;
    private String acquirer_response_msg;
    private String acquirer_authorization_code;
    private String created_timestamp;
    private String acquirer_created_timestamp;
    private String first_6;
    private String last_4;
    private String payer_name;
    private String exp_date;
    private String request_timestamp;
    private String request_mid;
    private String transaction_type;
    private String signature;

    public String getPayer_id() {
        return payer_id;
    }

    public void setPayer_id(String payer_id) {
        this.payer_id = payer_id;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getAcquirer_transaction_id() {
        return acquirer_transaction_id;
    }

    public void setAcquirer_transaction_id(String acquirer_transaction_id) {
        this.acquirer_transaction_id = acquirer_transaction_id;
    }

    public String getRequest_amount() {
        return request_amount;
    }

    public void setRequest_amount(String request_amount) {
        this.request_amount = request_amount;
    }

    public String getRequest_ccy() {
        return request_ccy;
    }

    public void setRequest_ccy(String request_ccy) {
        this.request_ccy = request_ccy;
    }

    public String getAuthorized_amount() {
        return authorized_amount;
    }

    public void setAuthorized_amount(String authorized_amount) {
        this.authorized_amount = authorized_amount;
    }

    public String getAuthorized_ccy() {
        return authorized_ccy;
    }

    public void setAuthorized_ccy(String authorized_ccy) {
        this.authorized_ccy = authorized_ccy;
    }

    public String getAcquirer_authorized_amount() {
        return acquirer_authorized_amount;
    }

    public void setAcquirer_authorized_amount(String acquirer_authorized_amount) {
        this.acquirer_authorized_amount = acquirer_authorized_amount;
    }

    public String getAcquirer_authorized_ccy() {
        return acquirer_authorized_ccy;
    }

    public void setAcquirer_authorized_ccy(String acquirer_authorized_ccy) {
        this.acquirer_authorized_ccy = acquirer_authorized_ccy;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getResponse_msg() {
        return response_msg;
    }

    public void setResponse_msg(String response_msg) {
        this.response_msg = response_msg;
    }

    public String getAcquirer_response_code() {
        return acquirer_response_code;
    }

    public void setAcquirer_response_code(String acquirer_response_code) {
        this.acquirer_response_code = acquirer_response_code;
    }

    public String getAcquirer_response_msg() {
        return acquirer_response_msg;
    }

    public void setAcquirer_response_msg(String acquirer_response_msg) {
        this.acquirer_response_msg = acquirer_response_msg;
    }

    public String getAcquirer_authorization_code() {
        return acquirer_authorization_code;
    }

    public void setAcquirer_authorization_code(String acquirer_authorization_code) {
        this.acquirer_authorization_code = acquirer_authorization_code;
    }

    public String getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(String created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public String getAcquirer_created_timestamp() {
        return acquirer_created_timestamp;
    }

    public void setAcquirer_created_timestamp(String acquirer_created_timestamp) {
        this.acquirer_created_timestamp = acquirer_created_timestamp;
    }

    public String getFirst_6() {
        return first_6;
    }

    public void setFirst_6(String first_6) {
        this.first_6 = first_6;
    }

    public String getLast_4() {
        return last_4;
    }

    public void setLast_4(String last_4) {
        this.last_4 = last_4;
    }

    public String getPayer_name() {
        return payer_name;
    }

    public void setPayer_name(String payer_name) {
        this.payer_name = payer_name;
    }

    public String getExp_date() {
        return exp_date;
    }

    public void setExp_date(String exp_date) {
        this.exp_date = exp_date;
    }

    public String getRequest_timestamp() {
        return request_timestamp;
    }

    public void setRequest_timestamp(String request_timestamp) {
        this.request_timestamp = request_timestamp;
    }

    public String getRequest_mid() {
        return request_mid;
    }

    public void setRequest_mid(String request_mid) {
        this.request_mid = request_mid;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}

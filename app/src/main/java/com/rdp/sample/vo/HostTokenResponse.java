package com.rdp.sample.vo;

/**
 * @author yb
 * @date 2018/3/6
 */
public class HostTokenResponse {

    /**
     * created_timestamp : 1520321458
     * expired_timestamp : 1520407858
     * mid : 0000021009
     * order_id : 1520321414629
     * transaction_id : 1520321414629
     * payment_url : https://secure-dev.reddotpayment.com/service/token/--SERVER--/ac15af67ac3a773ae8da51d1951f769927208e67a2e003009f20eabf9bdda4704f7143b523a380f5101c20f02ff3793ad8ef001f62fb2001892e584bafb3682d
     * response_code : 0
     * response_msg : successful
     * signature : 3034a0c622a34193325738434eb50cc02b4ff1d6d6e69cebea6167804163ec4c4939946b1d3e67e8ac48c9c87edbbe3ca56fe77bfd35292ae9f6eef4e9ca332c
     */

    private long created_timestamp;
    private long expired_timestamp;
    private String mid;
    private String order_id;
    private String transaction_id;
    private String payment_url;
    private int response_code;
    private String response_msg;
    private String signature;

    public long getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(long created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public long getExpired_timestamp() {
        return expired_timestamp;
    }

    public void setExpired_timestamp(long expired_timestamp) {
        this.expired_timestamp = expired_timestamp;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getPayment_url() {
        return payment_url;
    }

    public void setPayment_url(String payment_url) {
        this.payment_url = payment_url;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getResponse_msg() {
        return response_msg;
    }

    public void setResponse_msg(String response_msg) {
        this.response_msg = response_msg;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}

package com.rdp.sample.vo;

/**
 * @author yb
 * @date 2018/3/6
 */
public class DirectTokenResponse {

    /**
     * mid : 0000021009
     * transaction_id : 1520314344956
     * order_id : 1520314344956
     * token_id : 1069283020461111
     * acquirer_response_msg : OK
     * payer_id : 1069283020461111
     * created_timestamp : 2018-03-06 13:32:27
     * response_code : 0
     * response_msg : OK
     * payer_name : TPayer TPayer
     * first_6 : 411111
     * last_4 : 1111
     * exp_date : 122022
     * request_timestamp : 2018-03-06 13:32:25
     * payer_email : 40722@qq.com
     * transaction_type : C
     * signature : 6c106dbca5e1e123a899d8a9ab8f977ed4f50967b2dfeb2cf3eb4eb7242c4310cac9e946030653609954a0902c8e77b1db2fc1a837838d13073d3eaf3909d42d
     */

    private String mid;
    private String transaction_id;
    private String order_id;
    private String token_id;
    private String acquirer_response_msg;
    private String payer_id;
    private String created_timestamp;
    private int response_code;
    private String response_msg;
    private String payer_name;
    private String first_6;
    private String last_4;
    private String exp_date;
    private String request_timestamp;
    private String payer_email;
    private String transaction_type;
    private String signature;

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getAcquirer_response_msg() {
        return acquirer_response_msg;
    }

    public void setAcquirer_response_msg(String acquirer_response_msg) {
        this.acquirer_response_msg = acquirer_response_msg;
    }

    public String getPayer_id() {
        return payer_id;
    }

    public void setPayer_id(String payer_id) {
        this.payer_id = payer_id;
    }

    public String getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(String created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getResponse_msg() {
        return response_msg;
    }

    public void setResponse_msg(String response_msg) {
        this.response_msg = response_msg;
    }

    public String getPayer_name() {
        return payer_name;
    }

    public void setPayer_name(String payer_name) {
        this.payer_name = payer_name;
    }

    public String getFirst_6() {
        return first_6;
    }

    public void setFirst_6(String first_6) {
        this.first_6 = first_6;
    }

    public String getLast_4() {
        return last_4;
    }

    public void setLast_4(String last_4) {
        this.last_4 = last_4;
    }

    public String getExp_date() {
        return exp_date;
    }

    public void setExp_date(String exp_date) {
        this.exp_date = exp_date;
    }

    public String getRequest_timestamp() {
        return request_timestamp;
    }

    public void setRequest_timestamp(String request_timestamp) {
        this.request_timestamp = request_timestamp;
    }

    public String getPayer_email() {
        return payer_email;
    }

    public void setPayer_email(String payer_email) {
        this.payer_email = payer_email;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}

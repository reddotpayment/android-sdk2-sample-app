package com.rdp.sample.vo;

/**
 * @author yb
 * @date 2018/3/7
 */
public class RedirectPaymentResponse {

    /**
     * created_timestamp : 1520415434
     * expired_timestamp : 1520501834
     * mid : 0000021009
     * order_id : 1520415432510
     * transaction_id : 1520415432510_162989043682159804
     * payment_url : https://secure-dev.reddotpayment.com/service/payment/--SERVER--/77936985e18aae443ece8cb9df7185d3640c89385912b3af7c6c2e499f0d716893c6e878cf29d6e8d670575eb09c2c841bd0c94d8876f70eb19639546aea26b6
     * response_code : 0
     * response_msg : successful
     * signature : 54e4bcb8c4382e17638423e3290cc577dac06205fb9a89b89e364acb2e9a847cf48f18a179d1273c2dfbe2ed40c359d44dda29266039a9b269da13be5ae6a707
     */

    private long created_timestamp;
    private long expired_timestamp;
    private String mid;
    private String order_id;
    private String transaction_id;
    private String payment_url;
    private int response_code;
    private String response_msg;
    private String signature;

    public long getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(long created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public long getExpired_timestamp() {
        return expired_timestamp;
    }

    public void setExpired_timestamp(long expired_timestamp) {
        this.expired_timestamp = expired_timestamp;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getPayment_url() {
        return payment_url;
    }

    public void setPayment_url(String payment_url) {
        this.payment_url = payment_url;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getResponse_msg() {
        return response_msg;
    }

    public void setResponse_msg(String response_msg) {
        this.response_msg = response_msg;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}

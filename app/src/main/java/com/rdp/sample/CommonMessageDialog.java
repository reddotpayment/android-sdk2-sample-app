package com.rdp.sample;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;

import com.rdp.sample.databinding.LayoutCommonMessageDialogBinding;

/**
 * @author 40722
 * @date 2016/7/20
 */
public class CommonMessageDialog {
    private Context context;
    private String message;
    private String title;
    private Dialog dialog;

    public Dialog getDialog() {
        return dialog;
    }

    public CommonMessageDialog(Context context, String message, String title) {
        this.context = context;
        this.message = message;
        this.title = title;
    }

    public void show() {
        LayoutCommonMessageDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_common_message_dialog, null, false);
        dialog = Helper.showCustomDialog(context, binding.getRoot(), 0, Gravity.CENTER);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        binding.message.setText(message);

        if (!TextUtils.isEmpty(title)) {
            binding.title.setText(title);
        }

        binding.rightClick.setOnClickListener(view -> {

            dialog.dismiss();
        });
    }
}

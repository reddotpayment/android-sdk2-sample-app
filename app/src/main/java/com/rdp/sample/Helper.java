package com.rdp.sample;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * @author yb
 * @date 2018/3/8
 */
public class Helper {

    public static Dialog showCustomDialog(Context context, View contentView, int styleResId, int gravity) {
        Dialog dialog = new Dialog(context, R.style.Theme_FloatActivity);
        dialog.setContentView(contentView);
        showCustomDialog(dialog, styleResId, gravity);
        return dialog;
    }
    private static void showCustomDialog(Dialog dialog, int styleResId, int gravity) {
        Window dialogWindow = dialog.getWindow();
        if (dialogWindow != null) {
            dialogWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialogWindow.setGravity(gravity);
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.copyFrom(dialogWindow.getAttributes());
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            if (styleResId != 0) {
                layoutParams.windowAnimations = styleResId;
            }
            dialogWindow.setAttributes(layoutParams);
        }
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }
}
